# ESP Web Log Parser

This repository contains the source to build a docker image that provides services from the ESP core code base running on a CentOS 7 base image.  It exposes a web service that allows users to submit ESP .log files for parsing.

## Building the Image

The ESP core code is kept in a private GitHub repository located [here](https://github.com/MBARI-ESP/ESP2Gscript):

Because this project is private, you need to have access to that GitHub repo in order to use this docker service.  Once you have access to the project, you can continue with these instructions. When you are ready to move forward, identify a location on your hard drive where you will store all the data that lives outside the container.  For these instructions, we will call this HOST_BASE_DIR.  Once you have this location, open a terminal and navigate to the HOST_BASE_DIR directory and then run:

```bash
git clone https://github.com/MBARI-ESP/ESP2Gscript.git
```

This will create a ESP2GScript directory which we will mount as a volume into the docker container.  This is done so that you can keep the ESP core codebase up to date by running a git pull from the repo outside the container which will make the updated files available inside the container.  You can also create a couple of directories to store logs and other data from the container services.  Create these directories:

```bash
HOST_BASE_DIR/esp-web-log-parser/logs
HOST_BASE_DIR/esp-web-log-parser/uploads
```

Once this is done, in the terminal, cd back to the base directory which contains these instructions and the Dockerfile.  Then build the docker image using the following command:

```bash
docker build -t mbari/esp-web-log-parser .
```

This will build the mbari/esp-web-log-parser Docker image locally with the code that is available in this repository.  You can skip the build step if you want and just pull the image from the DockerHub repository.  Either way, you can run the service using:

```bash
docker run \
  -v HOST_BASE_DIR/esp-web-log-parser/logs:/var/log/httpd \
  -v HOST_BASE_DIR/esp-web-log-parser/uploads:/data/uploads \
  -v HOST_BASE_DIR/ESP2Gscript:/home/esp/ESP2Gscript \
  -p 8080:80 mbari/esp-web-log-parser
```

Note for the above to work, HOST_BASE_DIR needs to be a fully qualified system path.

## Testing the service

Once the service is running, you can parse a log file over HTTP.  Here is an example call using curl

```bash
curl -X POST -F 'espname=jake' -F 'esptype=shallow' -F 'espmode=real' -F 'logfile=@/path/to/logfile.log' http://localhost:8080/cgi-bin/log-parse.cgi
```

or you can visit the web page [on your local machine](http://localhost:8080) and use the form to submit a file for parsing. Note this should just be for small log files, otherwise, it bogs the browser down.
